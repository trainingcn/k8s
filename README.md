# Kubernetes training

This repo contains basic examples for K8s beginners.

It helps on the introduction to K8s resources like:

* Pod

* Deployment

* RecordSet

* DaemonSet

* StatefulSet

* Services

* Ingress

* Namespaces

And RBAC concepts as well:

* ServiceAcccounts

* Role

* RoleBinding

# Required material

1. Install [kubectl](https://kubernetes.io/docs/tasks/tools/install-kubectl) - the K8s command line interface (CLI) at your workstation.

```curl -LO https://storage.googleapis.com/kubernetes-release/release/`curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt`/bin/linux/amd64```

```chmod +x ./kubectl```

```mv kubectl /usr/local/bin/kubectl```

2. 'KUBECONFIG' file

That is the configuration file for 'kubectl' that allows you to connect to a remote K8s cluster from your workstation

```mkdir ~/.kube && cp config.172.31.13.6.tango ~/.kube/config```

# How to contribute

Now, you can clone this repo and start you own path or you can fork this repo and then request a Merge Request with your add-ons.

